﻿Imports System.IO

Class MainWindow


    Private Sub Window_Initialized(sender As Object, e As EventArgs)
        If File.Exists("C:\testy\bin\config\Zamykaj.txt") = True Then
            ZamykanieATM.Background = Brushes.Green
        End If

        If File.Exists("C:\wyniki\UST\specjalne\WyborBaterii\AntycypacjaUkryta.txt") = True Then
            AntycypacjaUkryta.Background = Brushes.Green
        End If

        If File.Exists("C:\wyniki\UST\specjalne\WyborBaterii\EPQRUkryte.txt") = True Then
            EPQRUkryte.Background = Brushes.Green
        End If

        If File.Exists("C:\wyniki\UST\specjalne\WyborBaterii\FlagiUkryte.txt") = True Then
            FlagiUkryte.Background = Brushes.Green
        End If

        If File.Exists("C:\testy\bin\config\ATM\WersjaBezDanychOsobowych.txt") = True Then
            PomijanieDanychOsobowychATM.Background = Brushes.Green
        End If

        If File.Exists("C:\testy\bin\config\Testy\InstrukcjaDlaAtmF5.txt") = True Then
            F5Sluchawski.Background = Brushes.Green
        End If

        If File.Exists("C:\testy\bin\config\Testy\PokazPrzyciskExitDaneOsobowe.txt") = True Then
            KillATMDaneOsobowe.Background = Brushes.Green
        End If

        If File.Exists("C:\wyniki\UST\specjalne\WyborBaterii\UkryjPelneWersjeWWyborzeBaterii.txt") = True Then
            UkryjPelneWersjeWWyborzeBaterii.Background = Brushes.Green
        End If

        If File.Exists("C:\wyniki\UST\specjalne\WyborBaterii\BrakPrzerwyMiedzyTestami.txt") = True Then
            UnloadBreakBetweenTests.Background = Brushes.Green
        End If
    End Sub

    Private Sub StworzLubUsun(sciezka As String, label As Label)
        If Not File.Exists(sciezka) = True Then
            Dim sw As StreamWriter = New StreamWriter(sciezka)
            sw.Close()
        Else
            File.Delete(sciezka)
        End If

        If label.Background Is Brushes.Green Then
            label.Background = Brushes.Red
        Else
            label.Background = Brushes.Green
        End If
    End Sub

    Private Sub FlagiUkryte_MouseDown(sender As Object, e As MouseButtonEventArgs) Handles FlagiUkryte.MouseDown
        StworzLubUsun("C:\wyniki\UST\specjalne\WyborBaterii\FlagiUkryte.txt", CType(sender, Label))

    End Sub

    Private Sub EPQRUkryte_MouseDown(sender As Object, e As MouseButtonEventArgs) Handles EPQRUkryte.MouseDown
        StworzLubUsun("C:\wyniki\UST\specjalne\WyborBaterii\EPQRUkryte.txt", CType(sender, Label))
    End Sub

    Private Sub AntycypacjaUkryta_MouseDown(sender As Object, e As MouseButtonEventArgs) Handles AntycypacjaUkryta.MouseDown
        StworzLubUsun("C:\wyniki\UST\specjalne\WyborBaterii\AntycypacjaUkryta.txt", CType(sender, Label))
    End Sub

    Private Sub ZamykanieATM_MouseDown(sender As Object, e As MouseButtonEventArgs) Handles ZamykanieATM.MouseDown
        StworzLubUsun("C:\testy\bin\config\Zamykaj.txt", CType(sender, Label))
    End Sub

    Private Sub PomijanieDanychOsobowychATM_MouseDown(sender As Object, e As MouseButtonEventArgs) Handles PomijanieDanychOsobowychATM.MouseDown
        StworzLubUsun("C:\testy\bin\config\ATM\WersjaBezDanychOsobowych.txt", CType(sender, Label))
    End Sub

    Private Sub F5Sluchawski_MouseDown(sender As Object, e As MouseButtonEventArgs) Handles F5Sluchawski.MouseDown
        StworzLubUsun("C:\testy\bin\config\Testy\InstrukcjaDlaAtmF5.txt", CType(sender, Label))
    End Sub

    Private Sub KillATMDaneOsobowe_MouseDown(sender As Object, e As MouseButtonEventArgs) Handles KillATMDaneOsobowe.MouseDown
        StworzLubUsun("C:\testy\bin\config\Testy\PokazPrzyciskExitDaneOsobowe.txt", CType(sender, Label))
    End Sub

    Private Sub UkryjPelneWersjeWWyborzeBaterii_MouseDown(sender As Object, e As MouseButtonEventArgs) Handles UkryjPelneWersjeWWyborzeBaterii.MouseDown
        StworzLubUsun("C:\wyniki\UST\specjalne\WyborBaterii\UkryjPelneWersjeWWyborzeBaterii.txt", CType(sender, Label))
    End Sub

    Private Sub UnloadBreakBetweenTests_MouseDown(sender As Object, e As MouseButtonEventArgs) Handles UnloadBreakBetweenTests.MouseDown
        StworzLubUsun("C:\wyniki\UST\specjalne\WyborBaterii\BrakPrzerwyMiedzyTestami.txt", CType(sender, Label))
    End Sub

    Private Sub UkryjWszystkoOproczWatchKlawiatura_MouseDown(sender As Object, e As MouseButtonEventArgs) Handles UkryjWszystkoOproczWatchKlawiatura.MouseDown
       StworzLubUsun("C:\wyniki\UST\specjalne\WyborBaterii\UkryjWszystkoOproczWatchKlawiatura.txt", CType(sender, Label))
 
    End Sub
End Class
